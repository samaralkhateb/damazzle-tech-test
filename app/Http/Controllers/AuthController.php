<?php

namespace App\Http\Controllers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\User;
use Laravel\Passport\Client as OClient;
use Illuminate\Validation\ValidationException;


/**
 *
 * @group Auth Pages
 *
 * Apis related auth with user
 */

class AuthController extends Controller
{

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:4',
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);
        try {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);
        }
        catch (\Exception $e){
            return response()->json([
                'success' => false,
                'message' => 'Email Already Exist',
                ], 400);
        }


        $token = $user->createToken('LaravelAuthApp')->accessToken;

        return response()->json(['user'=>$user,'token' => $token], 200);
    }


    /**
     * @group Auth Pages
     * Login
     * @bodyParam user object required The user details
     * @bodyParam user.email string required The user's email Example:samar_kh@gmail.com
     * @bodyParam user.password string required The user's password Example:sam12324
     * @response{
     * "success": true,
     * "data": {
     * "user": {
     * "id": 4,
     * "name": "samar-kh",
     * "email": "samar_kh@gmail.com",
     * "email_verified_at": null,
     * "created_at": "2022-03-19T13:51:01.000000Z",
     * "updated_at": "2022-03-19T13:58:27.000000Z"
     * },
     * "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZTMxOWIxMGQ4OWNhM2Y1MWJhY2E1ZDQyMjc1ZWJkYzlhOWNlY2U4M2YxMmM5YTliOGU4ZjE5ZGMyMmQ4MzBhOTIwZGNlMDFiZDJjNTMxY2UiLCJpYXQiOjE2NDc3MDgyMDAsIm5iZiI6MTY0NzcwODIwMCwiZXhwIjoxNjc5MjQ0MjAwLCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.Y2paYInycK4hS13dnvH1pY8blWcHQKXslYq57xI9MDfZLaIbT8oMApUE0UQt1jSTyeWe63G4v-_GUmcociDUVeE48S2JKV4a7kjxyXFqnEig8CEOTev4hJsoQHT7uLdL5YpyCITGyvTXcJ9b3xHS1iwUbqJTh2RbT184mO-T1qfBYIri8FwUxD4Zr4j_O5t4B0rDUbyhjeqTKebkiuqbzp4kziKFAInaFJXDgjBQZ0qMixPVm6u5RjVgmcayB4O9vBaI81xBPbi5eWV-rvUPZZrrIFow6aNr_xhvQT4e7drryJHS6Vv-640b_ywo_EAkjy6NV597qvMOO215fZHDfc4yMkNXNR3Y5i8xR2aBJK_VpxWqJT5yMHi7bRGMh1tN7o-6x0N2OJZyIc6yJ4YQyZWXhlt0CR646exmVGRNeJPOTwFLUa-EygcJmSouOWt6zQIe4QGGCxdoIMuiCIpTJ9fCC0YpmRdLOSVzob0d5Bo7tjSElQ-l4Ip9zZEgO1-aRCKhz8lZ5cJgRnR73o6UQ39ZqdkjtzEY823l7YWsk8-y_1bnM_-JFq8XslB-F7pQYYqEnHjkhSUVEM7qYSkECc_nTPQen73QiLwBC2YmvoBO8VyDCwaP-_V8NOthD6aQWG-65CPtJlriwmYxgu54b6szSd85gPqU_hzEpEzrlxk"
     * }
     *}
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function login(Request $request): JsonResponse
    {


        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth()->attempt($data)) {
            $user = auth()->user();

            $token = auth()->user()->createToken('LaravelAuthApp')->accessToken;
            return response()->json([
            'success'   => true,
            'data'      =>['user' =>$user,'token' => $token]
            ]);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }


    }


    /**
     * @group Auth Pages
     * Logout
     *@header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMjlhNDhkZmEzZDcwNzA2YjAxZTU4OTBkNzQ0YTlmMTllNmQzOTkxMmE3NDY5YmJlMjc0NmRhNzk2YjM2Zjc4ZDJjNjU1NjkzNTEzNGNhYTciLCJpYXQiOjE2NDc2OTgzMjMsIm5iZiI6MTY0NzY5ODMyMywiZXhwIjoxNjc5MjM0MzIzLCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.VPM4uhh9KUuz88yfEd5ZOhDquoZ70RGwswOrpiNWrluke5l-0nfy_YE1IgLFPdKwWbyxj7eM9geUYTq7qtMB-pEgrFaXCu11QBsBH8Ek_tfVlOBVqoQ1h61M11kjHE6HZpkZkYvBx-f4YamNu_bwQk20foQquQmS-_3cW1gQDq00AaMYv3Jcx8Yz2sDztCBsaxcYUr35wSaKclaG8y7ywvBix80DLLToXk__KJN3lijDu56GgjnH-2-cXzgXsuWGREsA9T9MAf2BBThKSLFsw_yPe_Zyoa1N38MQriJV9Y4hNI150aO9cx5OQayjwYeps_4jYdorgUuetipVdVmm3oCvsxbC4OQse-e6MUy54CLi94qFF1fUkWyNNOxp1dFfXGnw2TdcePVCxH8wufCYwYDYHH2aog_pl2_WxYenCrZSjvFS-xKvGZaqEFPx8BFoGTzRGONKJ2IZDRBKCK1BlERVBZrMxOMvBhCCeHskJFmnCsRM13TBJ8ZopIQoCLLKjgUhoW3le03dkEkx8yfW_SddzxZ5oygmar5QZarfAmQsESpwSrx7QQ2k4PSKx7ncQH7beKbioMGFiE-TEon2kG_5ABdpyLQt747aztnKhmt00pRhitA-LC66Y6eO4JXVBbx9-9MAhgfZAfetEtGl0XqB54EFkAB9jya7HgU_DPU
     *@authenticated
     * @response {
        "success": true,
        "message": "Successfully logged out"
     }
     * @throws ValidationException
     */

    public function logout(Request $request): ?JsonResponse
    {
        $request->user()->token()->revoke();

        return response()->json([
            'success' => true,
            'message' => 'Successfully logged out',

        ]);
    }

    /**
     * @group Auth Pages
     * User Refresh Token
     * @bodyParam user object required The user details
     * @bodyParam user.email string required The user's email Example:samar_kh@gmail.com
     * @bodyParam user.password string required The user's password Example:sam12324
     * @response {
    "success": true,
    "data": {
    "token_type": "Bearer",
    "expires_in": 86400,
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMjA4NmJmYjg4ZDVhZWU3OWE1OTEwZjVkMmEwMGY4ZmZkOTdjMTAxODMzMmUzNDBjOTJhNGYzZGYzNzI1NTBlMjI1OGMzZmViN2NmZTA2ZWIiLCJpYXQiOjE2NDc3MDg2NTIsIm5iZiI6MTY0NzcwODY1MiwiZXhwIjoxNjQ3Nzk1MDUyLCJzdWIiOiIxIiwic2NvcGVzIjpbIioiXX0.IMnhCoYWg19X6gnBX4H79Lpb33in20lFNqSk9fV1oD9ov6WGx4PbiZ062L71i_P9hEvApT1jNs_fnRvuFckrWfOYHl7RpqiEUUc6Z6BBAOEJNQSSqHlcvbyG31lw0TZ9J-ENPo8E4iwuV3Y2xLA5COHvC52SE14_FnwrPBQSRp_xtn1ERSGaa8s2aXLG0EkerIiJnqwR7ISHfIkB8_mJD3k_4mruJfuwlaZqgkZl20-6_BP6xtlgfxK8OESN6_Oc5Qth7lBKqu-yfKbSlmxjzJdx3a2t5cJhOsIdBF266m9eHABGrIUxjdOIXnqFVILBXQ7z-OWgsbt6W8hAE16EsY8VK6CuROGMC63nfifS_QAKbVT4d9_8Fwut6K3FwWL5e2PJ_r2FonIN1JVzb4zlFcrcfKAqOvutLje6YUWubVyZe3hDH6pAhMPFUEOKsMEzA98i3TONoH4UOAjH1_WU-WAGQbZ0VGSOm4nzOC1kTpgdJoXfZ0loYm_Lk8TRPveXKTXb8Fz3LADSqFzcjpsuq843b18-37ljLrrYSn_FQPHeX9LjCnqOTQx8Kl4Ch6RW84-6g1htSQUUZnAR4jpOVPtrodRQRP5C-GEcAv6tNKzpc8dl5RK17xQz8vGNmvxt9GDAwawQO4N31fUsejHUZS7RYsBZGx8xNFKdo3DCmQA",
    "refresh_token": "def50200a0664fe2ede6f690964e4ddf72e7a27dccab103deb231752b3a9a4a7164cec6aa7a922fffe41a0a23b605f6338e4f188b5a4222c051dc221f9dd78df28c58ce1fb85c737906aa0dcb63c2fd3ee948b18160e161dd4003c893f1e35a8c94f1abe2776d746216c9ce984011f7c1c8d38f84686a52c75cea7e3d356405187a8b704dcdf419382c3018b439a068c9f3bedf7e0a74cf847a45e2500edc1a7520e5c27276f848bee0bc520af22e2ffd86c4a3553d45289e134ca9e7f13dcec8c976d72ed3565b5480e98aa4738aa403c2e56a86f39a3fdc97d5f93e915b52428724c3a5e848e6bd695d78747aca571cf80d35c60511ab759511da817d302ad57d59b0d5dab9b91709f105c7e510c9b929fac12a40230cb62db2a4571854f020c17836e29b85ee1015659a189bc09ad174b640df341e8b7adca5ffaf1a97cface57888b7dfdb95ab4720dfc1749f6015e2eea65e22432e7d3280a7a3c9d5e059b52eb8b"
    }
    }
     */
    public function userRefreshToken(Request $request): JsonResponse
    {

        $oClient = OClient::first();

        $data = [
            'grant_type' => 'password',
            'client_id' => $oClient->id,
            'client_secret' => $oClient->secret,
            'username' => $request->input('email'),
            'password' => $request->input('password'),
            'scope' => '*',
        ];
        $request = Request::create('/oauth/token', 'POST', $data);
        $content = json_decode(app()->handle($request)->getContent());

         return response()->json(['success'=>true , 'data' => $content]);
    }



}