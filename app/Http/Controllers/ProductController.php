<?php
namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 *
 * @group CRUD-Product
 *
 * Apis To Edit,Show,Create,Delete Products
 */


class ProductController extends Controller

{

    /**
     * @group CRUD-Product
     * Display a listing of Products.
     *@header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMjlhNDhkZmEzZDcwNzA2YjAxZTU4OTBkNzQ0YTlmMTllNmQzOTkxMmE3NDY5YmJlMjc0NmRhNzk2YjM2Zjc4ZDJjNjU1NjkzNTEzNGNhYTciLCJpYXQiOjE2NDc2OTgzMjMsIm5iZiI6MTY0NzY5ODMyMywiZXhwIjoxNjc5MjM0MzIzLCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.VPM4uhh9KUuz88yfEd5ZOhDquoZ70RGwswOrpiNWrluke5l-0nfy_YE1IgLFPdKwWbyxj7eM9geUYTq7qtMB-pEgrFaXCu11QBsBH8Ek_tfVlOBVqoQ1h61M11kjHE6HZpkZkYvBx-f4YamNu_bwQk20foQquQmS-_3cW1gQDq00AaMYv3Jcx8Yz2sDztCBsaxcYUr35wSaKclaG8y7ywvBix80DLLToXk__KJN3lijDu56GgjnH-2-cXzgXsuWGREsA9T9MAf2BBThKSLFsw_yPe_Zyoa1N38MQriJV9Y4hNI150aO9cx5OQayjwYeps_4jYdorgUuetipVdVmm3oCvsxbC4OQse-e6MUy54CLi94qFF1fUkWyNNOxp1dFfXGnw2TdcePVCxH8wufCYwYDYHH2aog_pl2_WxYenCrZSjvFS-xKvGZaqEFPx8BFoGTzRGONKJ2IZDRBKCK1BlERVBZrMxOMvBhCCeHskJFmnCsRM13TBJ8ZopIQoCLLKjgUhoW3le03dkEkx8yfW_SddzxZ5oygmar5QZarfAmQsESpwSrx7QQ2k4PSKx7ncQH7beKbioMGFiE-TEon2kG_5ABdpyLQt747aztnKhmt00pRhitA-LC66Y6eO4JXVBbx9-9MAhgfZAfetEtGl0XqB54EFkAB9jya7HgU_DPU
     *@authenticated
     * @response{
            "success": true,
            "data": [
                        {
                        "id": 1,
                        "user_id": 4,
                        "name": "product-1",
                        "description": "des product-1",
                        "image": "20220319141043.jpg",
                        "created_at": "2022-03-19T14:03:55.000000Z",
                        "updated_at": "2022-03-19T14:10:43.000000Z"
                        },
                        {
                        "id": 2,
                        "user_id": 4,
                        "name": "product-2",
                        "description": "des product-2",
                        "image": "20220319141028.jpg",
                        "created_at": "2022-03-19T14:05:03.000000Z",
                        "updated_at": "2022-03-19T14:10:28.000000Z"
                        }
                    ]
            }
     */

    public function index(): JsonResponse

    {

//        $products = Product::latest()->paginate(5);
        $products = Product::all();
        return response()->json([
            'success' => true,
            'data' => $products
        ]);

    }


    /**
     * @group CRUD-Product
     * Create New Products.
     *@header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMjlhNDhkZmEzZDcwNzA2YjAxZTU4OTBkNzQ0YTlmMTllNmQzOTkxMmE3NDY5YmJlMjc0NmRhNzk2YjM2Zjc4ZDJjNjU1NjkzNTEzNGNhYTciLCJpYXQiOjE2NDc2OTgzMjMsIm5iZiI6MTY0NzY5ODMyMywiZXhwIjoxNjc5MjM0MzIzLCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.VPM4uhh9KUuz88yfEd5ZOhDquoZ70RGwswOrpiNWrluke5l-0nfy_YE1IgLFPdKwWbyxj7eM9geUYTq7qtMB-pEgrFaXCu11QBsBH8Ek_tfVlOBVqoQ1h61M11kjHE6HZpkZkYvBx-f4YamNu_bwQk20foQquQmS-_3cW1gQDq00AaMYv3Jcx8Yz2sDztCBsaxcYUr35wSaKclaG8y7ywvBix80DLLToXk__KJN3lijDu56GgjnH-2-cXzgXsuWGREsA9T9MAf2BBThKSLFsw_yPe_Zyoa1N38MQriJV9Y4hNI150aO9cx5OQayjwYeps_4jYdorgUuetipVdVmm3oCvsxbC4OQse-e6MUy54CLi94qFF1fUkWyNNOxp1dFfXGnw2TdcePVCxH8wufCYwYDYHH2aog_pl2_WxYenCrZSjvFS-xKvGZaqEFPx8BFoGTzRGONKJ2IZDRBKCK1BlERVBZrMxOMvBhCCeHskJFmnCsRM13TBJ8ZopIQoCLLKjgUhoW3le03dkEkx8yfW_SddzxZ5oygmar5QZarfAmQsESpwSrx7QQ2k4PSKx7ncQH7beKbioMGFiE-TEon2kG_5ABdpyLQt747aztnKhmt00pRhitA-LC66Y6eO4JXVBbx9-9MAhgfZAfetEtGl0XqB54EFkAB9jya7HgU_DPU
     *@authenticated
     * @bodyParam product object required The user details
     * @bodyParam product.name string required The product's name
     * @bodyParam product.description string required The product's description
     * @bodyParam product.image file required|image|mimes:jpeg,png,jpg,gif,svg|max:2048 The product's image

     * @return JsonResponse
     * @response {
                "success": true,
                "data": {
                    "name": "product-1",
                    "description": "product 1",
                    "image": "20220319165954.png",
                    "user_id": 4,
                    "updated_at": "2022-03-19T16:59:54.000000Z",
                    "created_at": "2022-03-19T16:59:54.000000Z",
                    "id": 3
                }
    }
     **/


    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        $user = auth()->user();
        $input = $request->all();

        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }
        $input['user_id'] = $user['id'];

        $product = Product::create($input);

        return response()->json([
            'success' => true,
            'data' => $product
        ]);

    }


    /**
     * @group CRUD-Product
     * Display the specified resource.
     *@header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMjlhNDhkZmEzZDcwNzA2YjAxZTU4OTBkNzQ0YTlmMTllNmQzOTkxMmE3NDY5YmJlMjc0NmRhNzk2YjM2Zjc4ZDJjNjU1NjkzNTEzNGNhYTciLCJpYXQiOjE2NDc2OTgzMjMsIm5iZiI6MTY0NzY5ODMyMywiZXhwIjoxNjc5MjM0MzIzLCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.VPM4uhh9KUuz88yfEd5ZOhDquoZ70RGwswOrpiNWrluke5l-0nfy_YE1IgLFPdKwWbyxj7eM9geUYTq7qtMB-pEgrFaXCu11QBsBH8Ek_tfVlOBVqoQ1h61M11kjHE6HZpkZkYvBx-f4YamNu_bwQk20foQquQmS-_3cW1gQDq00AaMYv3Jcx8Yz2sDztCBsaxcYUr35wSaKclaG8y7ywvBix80DLLToXk__KJN3lijDu56GgjnH-2-cXzgXsuWGREsA9T9MAf2BBThKSLFsw_yPe_Zyoa1N38MQriJV9Y4hNI150aO9cx5OQayjwYeps_4jYdorgUuetipVdVmm3oCvsxbC4OQse-e6MUy54CLi94qFF1fUkWyNNOxp1dFfXGnw2TdcePVCxH8wufCYwYDYHH2aog_pl2_WxYenCrZSjvFS-xKvGZaqEFPx8BFoGTzRGONKJ2IZDRBKCK1BlERVBZrMxOMvBhCCeHskJFmnCsRM13TBJ8ZopIQoCLLKjgUhoW3le03dkEkx8yfW_SddzxZ5oygmar5QZarfAmQsESpwSrx7QQ2k4PSKx7ncQH7beKbioMGFiE-TEon2kG_5ABdpyLQt747aztnKhmt00pRhitA-LC66Y6eO4JXVBbx9-9MAhgfZAfetEtGl0XqB54EFkAB9jya7HgU_DPU
     *@authenticated
     * @urlParam id integer required The ID of the Product.
     * @return JsonResponse
     * @response {
            "success": true,
            "data": {
                "name": "product-1",
                "description": "product 1",
                "image": "20220319165954.png",
                "user_id": 4,
                "updated_at": "2022-03-19T16:59:54.000000Z",
                "created_at": "2022-03-19T16:59:54.000000Z",
                "id": 3
            }
    }
     */


    public function show($id): JsonResponse
    {
        $product = Product::find($id);

        if($product){
            $image = $product['image'];
            $product['image_path'] =  public_path("/image/").$image;
            return response()->json([  'success' => true, 'data' => $product  ]);
        }
        else
            return response()->json(['success' => false, 'message' => 'Product Not Found' ]);


    }

    /**
     * @group CRUD-Product
     * Update the specified resource in storage.
     *@header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMjlhNDhkZmEzZDcwNzA2YjAxZTU4OTBkNzQ0YTlmMTllNmQzOTkxMmE3NDY5YmJlMjc0NmRhNzk2YjM2Zjc4ZDJjNjU1NjkzNTEzNGNhYTciLCJpYXQiOjE2NDc2OTgzMjMsIm5iZiI6MTY0NzY5ODMyMywiZXhwIjoxNjc5MjM0MzIzLCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.VPM4uhh9KUuz88yfEd5ZOhDquoZ70RGwswOrpiNWrluke5l-0nfy_YE1IgLFPdKwWbyxj7eM9geUYTq7qtMB-pEgrFaXCu11QBsBH8Ek_tfVlOBVqoQ1h61M11kjHE6HZpkZkYvBx-f4YamNu_bwQk20foQquQmS-_3cW1gQDq00AaMYv3Jcx8Yz2sDztCBsaxcYUr35wSaKclaG8y7ywvBix80DLLToXk__KJN3lijDu56GgjnH-2-cXzgXsuWGREsA9T9MAf2BBThKSLFsw_yPe_Zyoa1N38MQriJV9Y4hNI150aO9cx5OQayjwYeps_4jYdorgUuetipVdVmm3oCvsxbC4OQse-e6MUy54CLi94qFF1fUkWyNNOxp1dFfXGnw2TdcePVCxH8wufCYwYDYHH2aog_pl2_WxYenCrZSjvFS-xKvGZaqEFPx8BFoGTzRGONKJ2IZDRBKCK1BlERVBZrMxOMvBhCCeHskJFmnCsRM13TBJ8ZopIQoCLLKjgUhoW3le03dkEkx8yfW_SddzxZ5oygmar5QZarfAmQsESpwSrx7QQ2k4PSKx7ncQH7beKbioMGFiE-TEon2kG_5ABdpyLQt747aztnKhmt00pRhitA-LC66Y6eO4JXVBbx9-9MAhgfZAfetEtGl0XqB54EFkAB9jya7HgU_DPU
     *@authenticated

     * @bodyParam product id string required The product's id
     * @bodyParam product object required The user details
     * @bodyParam product.name string required The product's name
     * @bodyParam product.description string required The product's description
     * @bodyParam product.image file image|mimes:jpeg,png,jpg,gif,svg|max:2048 The product's image
     * @return JsonResponse
     * @response {
            "success": true,
            "data": {
                "id": 2,
                "user_id": 4,
                "name": "product-2",
                "description": "des product-2",
                "image": "20220319141028.jpg",
                "created_at": "2022-03-19T14:05:03.000000Z",
                "updated_at": "2022-03-19T14:10:28.000000Z",
                "image_path": "/Applications/XAMPP/xamppfiles/htdocs/laravel-auth-master/public/image/20220319141028.jpg"
            }
    }
     **/

    public function update(Request $request, $id): JsonResponse
    {

        $request->validate([
            'name' => 'string',
            'description' => 'string',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        $product = Product::find($id);
        if(!$product)
            return response()->json(['success' => false, 'message' => 'Product Not Found' ]);


        $user = auth()->user();
        $input = $request->all();

        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }

        else{
            unset($input['image']);
        }
        $input['user_id'] = $user['id'];


        $product->update($input);
        return response()->json([
            'success' => true,
            'data' => $product
        ]);

    }


    /**
     * @group CRUD-Product
     * Remove the specified resource from storage.
     *@header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMjlhNDhkZmEzZDcwNzA2YjAxZTU4OTBkNzQ0YTlmMTllNmQzOTkxMmE3NDY5YmJlMjc0NmRhNzk2YjM2Zjc4ZDJjNjU1NjkzNTEzNGNhYTciLCJpYXQiOjE2NDc2OTgzMjMsIm5iZiI6MTY0NzY5ODMyMywiZXhwIjoxNjc5MjM0MzIzLCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.VPM4uhh9KUuz88yfEd5ZOhDquoZ70RGwswOrpiNWrluke5l-0nfy_YE1IgLFPdKwWbyxj7eM9geUYTq7qtMB-pEgrFaXCu11QBsBH8Ek_tfVlOBVqoQ1h61M11kjHE6HZpkZkYvBx-f4YamNu_bwQk20foQquQmS-_3cW1gQDq00AaMYv3Jcx8Yz2sDztCBsaxcYUr35wSaKclaG8y7ywvBix80DLLToXk__KJN3lijDu56GgjnH-2-cXzgXsuWGREsA9T9MAf2BBThKSLFsw_yPe_Zyoa1N38MQriJV9Y4hNI150aO9cx5OQayjwYeps_4jYdorgUuetipVdVmm3oCvsxbC4OQse-e6MUy54CLi94qFF1fUkWyNNOxp1dFfXGnw2TdcePVCxH8wufCYwYDYHH2aog_pl2_WxYenCrZSjvFS-xKvGZaqEFPx8BFoGTzRGONKJ2IZDRBKCK1BlERVBZrMxOMvBhCCeHskJFmnCsRM13TBJ8ZopIQoCLLKjgUhoW3le03dkEkx8yfW_SddzxZ5oygmar5QZarfAmQsESpwSrx7QQ2k4PSKx7ncQH7beKbioMGFiE-TEon2kG_5ABdpyLQt747aztnKhmt00pRhitA-LC66Y6eO4JXVBbx9-9MAhgfZAfetEtGl0XqB54EFkAB9jya7HgU_DPU
     *@authenticated
     * @bodyParam product id string required The product's id
     * @return JsonResponse
     * @throws \Exception
     * @response{
            "success": true,
            "data": "Deleted Successfully"
        }
     **/

    public function destroy($id): JsonResponse

    {
         $product = Product::find($id);

        if($product){
            $product->delete();
            return response()->json([  'success' => true, 'data' => 'Deleted Successfully'  ]);
        }
        else
            return response()->json(['success' => false, 'message' => 'Product Not Found' ]);


    }



}