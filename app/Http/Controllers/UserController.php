<?php

namespace App\Http\Controllers;


use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Validator;


/**
 *
 * @group User-APIS
 *
 * Apis related auth with user
 */


class UserController extends Controller
{

    /**
     * @group User-APIS
     * Show User Profile
     *@header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMjlhNDhkZmEzZDcwNzA2YjAxZTU4OTBkNzQ0YTlmMTllNmQzOTkxMmE3NDY5YmJlMjc0NmRhNzk2YjM2Zjc4ZDJjNjU1NjkzNTEzNGNhYTciLCJpYXQiOjE2NDc2OTgzMjMsIm5iZiI6MTY0NzY5ODMyMywiZXhwIjoxNjc5MjM0MzIzLCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.VPM4uhh9KUuz88yfEd5ZOhDquoZ70RGwswOrpiNWrluke5l-0nfy_YE1IgLFPdKwWbyxj7eM9geUYTq7qtMB-pEgrFaXCu11QBsBH8Ek_tfVlOBVqoQ1h61M11kjHE6HZpkZkYvBx-f4YamNu_bwQk20foQquQmS-_3cW1gQDq00AaMYv3Jcx8Yz2sDztCBsaxcYUr35wSaKclaG8y7ywvBix80DLLToXk__KJN3lijDu56GgjnH-2-cXzgXsuWGREsA9T9MAf2BBThKSLFsw_yPe_Zyoa1N38MQriJV9Y4hNI150aO9cx5OQayjwYeps_4jYdorgUuetipVdVmm3oCvsxbC4OQse-e6MUy54CLi94qFF1fUkWyNNOxp1dFfXGnw2TdcePVCxH8wufCYwYDYHH2aog_pl2_WxYenCrZSjvFS-xKvGZaqEFPx8BFoGTzRGONKJ2IZDRBKCK1BlERVBZrMxOMvBhCCeHskJFmnCsRM13TBJ8ZopIQoCLLKjgUhoW3le03dkEkx8yfW_SddzxZ5oygmar5QZarfAmQsESpwSrx7QQ2k4PSKx7ncQH7beKbioMGFiE-TEon2kG_5ABdpyLQt747aztnKhmt00pRhitA-LC66Y6eO4JXVBbx9-9MAhgfZAfetEtGl0XqB54EFkAB9jya7HgU_DPU
     *@authenticated
     * @response{
        "success": true,
        "data": {
            "id": 4,
            "name": "samar-kh",
            "email": "samar_kh@gmail.com",
            "email_verified_at": null,
            "created_at": "2022-03-19T13:51:01.000000Z",
            "updated_at": "2022-03-19T13:58:27.000000Z"
        }
    }
     */
    public function showProfile(): ?JsonResponse
    {

        try {

            $user = auth()->user();

            return response()->json([
                'success' => true,
                'data'    => $user
            ]);

        }catch(\Exception $e){
            return response()->json(['error' => 'Unauthorised'], 401);
        }


    }


    /**
     * @group User-APIS
     * Edit User Profile
     * @bodyParam user object required The user details
     * @bodyParam user.email string  The user's email Example:samar_kh@gmail.com
     * @bodyParam user.name string  The user's name Example:samar
     * @header Authorization Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMjlhNDhkZmEzZDcwNzA2YjAxZTU4OTBkNzQ0YTlmMTllNmQzOTkxMmE3NDY5YmJlMjc0NmRhNzk2YjM2Zjc4ZDJjNjU1NjkzNTEzNGNhYTciLCJpYXQiOjE2NDc2OTgzMjMsIm5iZiI6MTY0NzY5ODMyMywiZXhwIjoxNjc5MjM0MzIzLCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.VPM4uhh9KUuz88yfEd5ZOhDquoZ70RGwswOrpiNWrluke5l-0nfy_YE1IgLFPdKwWbyxj7eM9geUYTq7qtMB-pEgrFaXCu11QBsBH8Ek_tfVlOBVqoQ1h61M11kjHE6HZpkZkYvBx-f4YamNu_bwQk20foQquQmS-_3cW1gQDq00AaMYv3Jcx8Yz2sDztCBsaxcYUr35wSaKclaG8y7ywvBix80DLLToXk__KJN3lijDu56GgjnH-2-cXzgXsuWGREsA9T9MAf2BBThKSLFsw_yPe_Zyoa1N38MQriJV9Y4hNI150aO9cx5OQayjwYeps_4jYdorgUuetipVdVmm3oCvsxbC4OQse-e6MUy54CLi94qFF1fUkWyNNOxp1dFfXGnw2TdcePVCxH8wufCYwYDYHH2aog_pl2_WxYenCrZSjvFS-xKvGZaqEFPx8BFoGTzRGONKJ2IZDRBKCK1BlERVBZrMxOMvBhCCeHskJFmnCsRM13TBJ8ZopIQoCLLKjgUhoW3le03dkEkx8yfW_SddzxZ5oygmar5QZarfAmQsESpwSrx7QQ2k4PSKx7ncQH7beKbioMGFiE-TEon2kG_5ABdpyLQt747aztnKhmt00pRhitA-LC66Y6eO4JXVBbx9-9MAhgfZAfetEtGl0XqB54EFkAB9jya7HgU_DPU
     * @authenticated
     * @response{
        "success": true,
        "message": "Your Profile Updated Successfully",
        "data": {
            "id": 4,
            "name": "samar-kh",
            "email": "samar_kh@gmail.com",
            "email_verified_at": null,
            "created_at": "2022-03-19T13:51:01.000000Z",
            "updated_at": "2022-03-19T13:58:27.000000Z"
        }
    }
     */
    public function editProfile(Request $request): ?JsonResponse
    {


        $user = auth()->user();
        $updated = $user->fill($request->all())->save();

        if ($updated)
            return response()->json([
                'success' => true,
                'message' => 'Your Profile Updated Successfully',
                'data'    => $user

            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Profile can not be updated'
            ], 500);
    }



}