<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Validation\ValidationException;

use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */


    public function render($request, Throwable $e)
    {

        if($e->getMessage() == 'Route [login] not defined.')
            return response()->json([ 'success' => false, 'message' => 'Not Authorization' ], 401);
        elseif ($e instanceof NotFoundHttpException)
            return response()->json([  'success' => false, 'message' => 'Not Found'   ], 404);
        elseif ($e instanceof ValidationException)
            return response()->json([ 'success' => false, 'message' => key($e->errors()) . " - ". current(current($e->errors())) ], 422);
//        else
//            return response()->json([  'success' => false, 'message' => 'General Error'  ], 500);

        return parent::render($request, $e);
    }

}
