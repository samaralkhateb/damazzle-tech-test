<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('register', [AuthController::class, 'register']);
Route::post('auth/login', [AuthController::class, 'login']);
Route::middleware('auth:api')->group(function () {

    Route::prefix('auth')->group(function() {

        Route::get('me', [UserController::class, 'showProfile']);
        Route::get('refresh', [AuthController::class, 'userRefreshToken']);
        Route::get('logout',  [AuthController::class, 'logout']);
        Route::post('me', [UserController::class, 'editProfile']);


    });
    Route::resource('products', ProductController::class);
});


