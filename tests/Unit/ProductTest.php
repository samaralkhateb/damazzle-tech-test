<?php

namespace Tests\Unit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    protected $headers = [];
    protected $user;
    protected $scopes = ['restricted-scope'];

    /**
     * @throws \Exception
     */
    public function testApiShowProduct() {

        $this->json('GET', '/api/auth/products',null,
            [
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiODYxODU3MzllZWE3Mzg3MzliOGNhZTFlM2Q2Zjk4YWVmZmVjOTgzYTEwYmIyOTNiMzliYTQ0MjMwZTM4NmQ4NTA0Zjc4NDZhZjcyOTFiNWYiLCJpYXQiOjE2NDc3MTU0NjcsIm5iZiI6MTY0NzcxNTQ2NywiZXhwIjoxNjc5MjUxNDY3LCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.aMc9gJ0atB_mm_pOkXfCHoKDPSRkGER6IZ4UcCFu92A8ReCrpuNROgDsmjPcXNUQsJJ1iFxLeR5nWj3X_A1wko1UhicGu5xTAQL9q3HBY1VXQ1pHaC3fjmByV34r--zvOzPuL27Bcw85MTj5rWjoYjuEvAjRJZcqd1YOha4p5HvXuD_aUd97OGqtHlutCqC8uHd7yFbPHZg2o7QEs16f1nHpsviUNtD_geElFfD3tCgotXUskh0VPPHRjmhO7DbvkAD0Jwtl2OVVZh6uuPtwl-r6U_aGgtobuyxH7jfaxvNYawO7Dr1Y0fI7Q6Q9DIMT4Ah7XfGkGVphCiMwO6gJn2C2nLOkgfhKXu_VGn5m-vuCvhhwZ_QByRSFR7eOIFR31kD9JhoXOn5Cs9a0_9dnS79GIkFIxZIe1douLImD4mQw-j_dcKBh_SNaXoUehl_e505HAJOFk7qXjpA-hIZEmG6GTMpveNNX493CcG1PENzY_HGiepTlHYxJoh2GmTVGZ95Mwu97FW7pTi83GEOxHzNEZmaBpt2_JvCDXV1Mya12kFEru71th5kb8W9kodsGczxdVFpfzx-1I4HEx53e9Mvpcfr8iY7nLcAkaf1L-yeLGXYJ52NvZALKXWF5fj7yc7Wah-7qtn1dDVUA0BWbP_o997dplW9sKmV0k029dv4'
            ]
        )
            ->assertStatus(200);
    }
    public function testApiShowProductById() {

        $this->json('GET', '/api/auth/products/1',null,
            [
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiODYxODU3MzllZWE3Mzg3MzliOGNhZTFlM2Q2Zjk4YWVmZmVjOTgzYTEwYmIyOTNiMzliYTQ0MjMwZTM4NmQ4NTA0Zjc4NDZhZjcyOTFiNWYiLCJpYXQiOjE2NDc3MTU0NjcsIm5iZiI6MTY0NzcxNTQ2NywiZXhwIjoxNjc5MjUxNDY3LCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.aMc9gJ0atB_mm_pOkXfCHoKDPSRkGER6IZ4UcCFu92A8ReCrpuNROgDsmjPcXNUQsJJ1iFxLeR5nWj3X_A1wko1UhicGu5xTAQL9q3HBY1VXQ1pHaC3fjmByV34r--zvOzPuL27Bcw85MTj5rWjoYjuEvAjRJZcqd1YOha4p5HvXuD_aUd97OGqtHlutCqC8uHd7yFbPHZg2o7QEs16f1nHpsviUNtD_geElFfD3tCgotXUskh0VPPHRjmhO7DbvkAD0Jwtl2OVVZh6uuPtwl-r6U_aGgtobuyxH7jfaxvNYawO7Dr1Y0fI7Q6Q9DIMT4Ah7XfGkGVphCiMwO6gJn2C2nLOkgfhKXu_VGn5m-vuCvhhwZ_QByRSFR7eOIFR31kD9JhoXOn5Cs9a0_9dnS79GIkFIxZIe1douLImD4mQw-j_dcKBh_SNaXoUehl_e505HAJOFk7qXjpA-hIZEmG6GTMpveNNX493CcG1PENzY_HGiepTlHYxJoh2GmTVGZ95Mwu97FW7pTi83GEOxHzNEZmaBpt2_JvCDXV1Mya12kFEru71th5kb8W9kodsGczxdVFpfzx-1I4HEx53e9Mvpcfr8iY7nLcAkaf1L-yeLGXYJ52NvZALKXWF5fj7yc7Wah-7qtn1dDVUA0BWbP_o997dplW9sKmV0k029dv4'
            ]
        )
            ->assertStatus(200);
    }

    public function testApiCreateProduct() {

        $data = array(
            'name'=>'product-1-1',
            'description'=>'product-1-1',
            'image' => 'test'
        );

        $this->json('POST', '/api/auth/products',$data,
            [
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiODYxODU3MzllZWE3Mzg3MzliOGNhZTFlM2Q2Zjk4YWVmZmVjOTgzYTEwYmIyOTNiMzliYTQ0MjMwZTM4NmQ4NTA0Zjc4NDZhZjcyOTFiNWYiLCJpYXQiOjE2NDc3MTU0NjcsIm5iZiI6MTY0NzcxNTQ2NywiZXhwIjoxNjc5MjUxNDY3LCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.aMc9gJ0atB_mm_pOkXfCHoKDPSRkGER6IZ4UcCFu92A8ReCrpuNROgDsmjPcXNUQsJJ1iFxLeR5nWj3X_A1wko1UhicGu5xTAQL9q3HBY1VXQ1pHaC3fjmByV34r--zvOzPuL27Bcw85MTj5rWjoYjuEvAjRJZcqd1YOha4p5HvXuD_aUd97OGqtHlutCqC8uHd7yFbPHZg2o7QEs16f1nHpsviUNtD_geElFfD3tCgotXUskh0VPPHRjmhO7DbvkAD0Jwtl2OVVZh6uuPtwl-r6U_aGgtobuyxH7jfaxvNYawO7Dr1Y0fI7Q6Q9DIMT4Ah7XfGkGVphCiMwO6gJn2C2nLOkgfhKXu_VGn5m-vuCvhhwZ_QByRSFR7eOIFR31kD9JhoXOn5Cs9a0_9dnS79GIkFIxZIe1douLImD4mQw-j_dcKBh_SNaXoUehl_e505HAJOFk7qXjpA-hIZEmG6GTMpveNNX493CcG1PENzY_HGiepTlHYxJoh2GmTVGZ95Mwu97FW7pTi83GEOxHzNEZmaBpt2_JvCDXV1Mya12kFEru71th5kb8W9kodsGczxdVFpfzx-1I4HEx53e9Mvpcfr8iY7nLcAkaf1L-yeLGXYJ52NvZALKXWF5fj7yc7Wah-7qtn1dDVUA0BWbP_o997dplW9sKmV0k029dv4'
            ]
        )
            ->assertStatus(200);
    }

    public function testApiEditProduct() {

        $data = array(
            'name'=>'product-1-1',
            'description'=>'product-1-1'
        );

        $this->json('POST', '/api/auth/products/3?_method=PATCH',$data,
            [
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiODYxODU3MzllZWE3Mzg3MzliOGNhZTFlM2Q2Zjk4YWVmZmVjOTgzYTEwYmIyOTNiMzliYTQ0MjMwZTM4NmQ4NTA0Zjc4NDZhZjcyOTFiNWYiLCJpYXQiOjE2NDc3MTU0NjcsIm5iZiI6MTY0NzcxNTQ2NywiZXhwIjoxNjc5MjUxNDY3LCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.aMc9gJ0atB_mm_pOkXfCHoKDPSRkGER6IZ4UcCFu92A8ReCrpuNROgDsmjPcXNUQsJJ1iFxLeR5nWj3X_A1wko1UhicGu5xTAQL9q3HBY1VXQ1pHaC3fjmByV34r--zvOzPuL27Bcw85MTj5rWjoYjuEvAjRJZcqd1YOha4p5HvXuD_aUd97OGqtHlutCqC8uHd7yFbPHZg2o7QEs16f1nHpsviUNtD_geElFfD3tCgotXUskh0VPPHRjmhO7DbvkAD0Jwtl2OVVZh6uuPtwl-r6U_aGgtobuyxH7jfaxvNYawO7Dr1Y0fI7Q6Q9DIMT4Ah7XfGkGVphCiMwO6gJn2C2nLOkgfhKXu_VGn5m-vuCvhhwZ_QByRSFR7eOIFR31kD9JhoXOn5Cs9a0_9dnS79GIkFIxZIe1douLImD4mQw-j_dcKBh_SNaXoUehl_e505HAJOFk7qXjpA-hIZEmG6GTMpveNNX493CcG1PENzY_HGiepTlHYxJoh2GmTVGZ95Mwu97FW7pTi83GEOxHzNEZmaBpt2_JvCDXV1Mya12kFEru71th5kb8W9kodsGczxdVFpfzx-1I4HEx53e9Mvpcfr8iY7nLcAkaf1L-yeLGXYJ52NvZALKXWF5fj7yc7Wah-7qtn1dDVUA0BWbP_o997dplW9sKmV0k029dv4'
            ]
        )
            ->assertStatus(200);
    }

    public function testApiDeleteProduct() {

        $this->json('DELETE', '/api/auth/products/2',null,
            [
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiODYxODU3MzllZWE3Mzg3MzliOGNhZTFlM2Q2Zjk4YWVmZmVjOTgzYTEwYmIyOTNiMzliYTQ0MjMwZTM4NmQ4NTA0Zjc4NDZhZjcyOTFiNWYiLCJpYXQiOjE2NDc3MTU0NjcsIm5iZiI6MTY0NzcxNTQ2NywiZXhwIjoxNjc5MjUxNDY3LCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.aMc9gJ0atB_mm_pOkXfCHoKDPSRkGER6IZ4UcCFu92A8ReCrpuNROgDsmjPcXNUQsJJ1iFxLeR5nWj3X_A1wko1UhicGu5xTAQL9q3HBY1VXQ1pHaC3fjmByV34r--zvOzPuL27Bcw85MTj5rWjoYjuEvAjRJZcqd1YOha4p5HvXuD_aUd97OGqtHlutCqC8uHd7yFbPHZg2o7QEs16f1nHpsviUNtD_geElFfD3tCgotXUskh0VPPHRjmhO7DbvkAD0Jwtl2OVVZh6uuPtwl-r6U_aGgtobuyxH7jfaxvNYawO7Dr1Y0fI7Q6Q9DIMT4Ah7XfGkGVphCiMwO6gJn2C2nLOkgfhKXu_VGn5m-vuCvhhwZ_QByRSFR7eOIFR31kD9JhoXOn5Cs9a0_9dnS79GIkFIxZIe1douLImD4mQw-j_dcKBh_SNaXoUehl_e505HAJOFk7qXjpA-hIZEmG6GTMpveNNX493CcG1PENzY_HGiepTlHYxJoh2GmTVGZ95Mwu97FW7pTi83GEOxHzNEZmaBpt2_JvCDXV1Mya12kFEru71th5kb8W9kodsGczxdVFpfzx-1I4HEx53e9Mvpcfr8iY7nLcAkaf1L-yeLGXYJ52NvZALKXWF5fj7yc7Wah-7qtn1dDVUA0BWbP_o997dplW9sKmV0k029dv4'
            ]
        )
            ->assertStatus(200);
    }
 }
