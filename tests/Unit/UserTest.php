<?php

namespace Tests\Unit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    protected $headers = [];
    protected $user;
    protected $scopes = ['restricted-scope'];

//
//    /**
//     * @throws \Exception
//     */
//    public function testApiLogin() {
//        $body = [
//
//            'email' => 'admin@admin.com',
//            'password' => 'admin',
//            'name' => 'admin'
//
//        ];
//        Request::create('/api/auth/login', 'POST', $body,['Accept' => 'application/json']);
//        $this->assertTrue(true);
//
//    }
//
//    public function testApiLogout() {
//
//        Request::create('/api/auth/logout', 'GET',[
//            'Accept'        => 'application/json',
//            'Authorization' => "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiY2M2NzhkZDE1MjM4ZmM2MTgxZTIwNmYwNGMyNjJlNTk5NjYzZmE0MDEzYWM2ZjA4MjIzNjJjM2JhZjA2MjRiMzMwNTU0ZjFmYjNmZmQ5NDgiLCJpYXQiOjE2NDc2MDAzMjEsIm5iZiI6MTY0NzYwMDMyMSwiZXhwIjoxNjc5MTM2MzIxLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.sp_x7wiQMVJJ0zI0vFgrEFvhoSR9xcQPb-FCsZ-y_L9Rs1gH4fIJGuEVLHBdMgTxIG3oapb_rhvISrgq4lMFPMFnTPvHcrGi3HfX12No393Hstg_pbTuAf1KvnOTDPSIeBKu9Wn_6Omucq5FNRnYIPqA0B8h1i9llauJLsfOajU4i2T8TvhIlfd1pnEMloDJi14bzoIyZyfQEUwlgHahxfsBJ806gwKgs9fKuJkp4e488pQOMFzIWgdtIt3s0Ab_mj3Gvw9jgKiVPP3NKePhiuhUsPQl2wi6y0xxw8oX8ToqWCraKGMKjKA1_SCIjpCMHhnG1O68u-qdJcECwg5V3zY3rsWG43RWXDGrKwGQiHhF-FRSd0qLDEJRo3JtVxecFu1Z0VCqZM38VXKhqVZfLeEkmSlU1imx_Ibdqn5ovJPnzNkXR6H5SL1Zu3y5foz9lmb-pEsDTzEq4gw3zRyh9NIL-2sRLBkpGooRYltqukO834QwSHc2Q5c-pV_O6210LSKFS2roJbvCDM1PH6p9w14_QlordzP8-QWELjwpG6CPzxYCCxu1zppQ65Im0SBUIlhi9I1mJIj3vCDuuLeSXU6NO0wX-GpL-zKBWFIVYU2Oe8KnrTOit6BlFjQrIncwwbE1QKp8TcjRSN2VCLjdNeDOMW0NEqo1_4Uvpqu96qM"
//        ]);
//        $this->assertTrue(true);
//
//    }



    public function testApiLogin() {
        $body = [
            'email' => 'samar_kh1@gmail.com',
            'password' => 'sam12324',
        ];
        $this->json('post', '/api/auth/login', $body)
            ->assertStatus(200);

    }

    public function testApiLogout() {

        $this->json('GET', '/api/auth/logout',[],
            [
                'Accept'        => 'application/json',
                'Authorization' => "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiODYxODU3MzllZWE3Mzg3MzliOGNhZTFlM2Q2Zjk4YWVmZmVjOTgzYTEwYmIyOTNiMzliYTQ0MjMwZTM4NmQ4NTA0Zjc4NDZhZjcyOTFiNWYiLCJpYXQiOjE2NDc3MTU0NjcsIm5iZiI6MTY0NzcxNTQ2NywiZXhwIjoxNjc5MjUxNDY3LCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.aMc9gJ0atB_mm_pOkXfCHoKDPSRkGER6IZ4UcCFu92A8ReCrpuNROgDsmjPcXNUQsJJ1iFxLeR5nWj3X_A1wko1UhicGu5xTAQL9q3HBY1VXQ1pHaC3fjmByV34r--zvOzPuL27Bcw85MTj5rWjoYjuEvAjRJZcqd1YOha4p5HvXuD_aUd97OGqtHlutCqC8uHd7yFbPHZg2o7QEs16f1nHpsviUNtD_geElFfD3tCgotXUskh0VPPHRjmhO7DbvkAD0Jwtl2OVVZh6uuPtwl-r6U_aGgtobuyxH7jfaxvNYawO7Dr1Y0fI7Q6Q9DIMT4Ah7XfGkGVphCiMwO6gJn2C2nLOkgfhKXu_VGn5m-vuCvhhwZ_QByRSFR7eOIFR31kD9JhoXOn5Cs9a0_9dnS79GIkFIxZIe1douLImD4mQw-j_dcKBh_SNaXoUehl_e505HAJOFk7qXjpA-hIZEmG6GTMpveNNX493CcG1PENzY_HGiepTlHYxJoh2GmTVGZ95Mwu97FW7pTi83GEOxHzNEZmaBpt2_JvCDXV1Mya12kFEru71th5kb8W9kodsGczxdVFpfzx-1I4HEx53e9Mvpcfr8iY7nLcAkaf1L-yeLGXYJ52NvZALKXWF5fj7yc7Wah-7qtn1dDVUA0BWbP_o997dplW9sKmV0k029dv4"
            ]
        )
            ->assertStatus(200);

    }

    public function testApiShowProfile() {

        $this->json('GET', '/api/auth/me',[],
            [
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiODYxODU3MzllZWE3Mzg3MzliOGNhZTFlM2Q2Zjk4YWVmZmVjOTgzYTEwYmIyOTNiMzliYTQ0MjMwZTM4NmQ4NTA0Zjc4NDZhZjcyOTFiNWYiLCJpYXQiOjE2NDc3MTU0NjcsIm5iZiI6MTY0NzcxNTQ2NywiZXhwIjoxNjc5MjUxNDY3LCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.aMc9gJ0atB_mm_pOkXfCHoKDPSRkGER6IZ4UcCFu92A8ReCrpuNROgDsmjPcXNUQsJJ1iFxLeR5nWj3X_A1wko1UhicGu5xTAQL9q3HBY1VXQ1pHaC3fjmByV34r--zvOzPuL27Bcw85MTj5rWjoYjuEvAjRJZcqd1YOha4p5HvXuD_aUd97OGqtHlutCqC8uHd7yFbPHZg2o7QEs16f1nHpsviUNtD_geElFfD3tCgotXUskh0VPPHRjmhO7DbvkAD0Jwtl2OVVZh6uuPtwl-r6U_aGgtobuyxH7jfaxvNYawO7Dr1Y0fI7Q6Q9DIMT4Ah7XfGkGVphCiMwO6gJn2C2nLOkgfhKXu_VGn5m-vuCvhhwZ_QByRSFR7eOIFR31kD9JhoXOn5Cs9a0_9dnS79GIkFIxZIe1douLImD4mQw-j_dcKBh_SNaXoUehl_e505HAJOFk7qXjpA-hIZEmG6GTMpveNNX493CcG1PENzY_HGiepTlHYxJoh2GmTVGZ95Mwu97FW7pTi83GEOxHzNEZmaBpt2_JvCDXV1Mya12kFEru71th5kb8W9kodsGczxdVFpfzx-1I4HEx53e9Mvpcfr8iY7nLcAkaf1L-yeLGXYJ52NvZALKXWF5fj7yc7Wah-7qtn1dDVUA0BWbP_o997dplW9sKmV0k029dv4'
            ]
        )
            ->assertStatus(200);

    }

    public function testApiEditProfile() {

        $this->json('POST', '/api/auth/me', ['name'=> 'samar-kh']
            ,
            [
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiODYxODU3MzllZWE3Mzg3MzliOGNhZTFlM2Q2Zjk4YWVmZmVjOTgzYTEwYmIyOTNiMzliYTQ0MjMwZTM4NmQ4NTA0Zjc4NDZhZjcyOTFiNWYiLCJpYXQiOjE2NDc3MTU0NjcsIm5iZiI6MTY0NzcxNTQ2NywiZXhwIjoxNjc5MjUxNDY3LCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.aMc9gJ0atB_mm_pOkXfCHoKDPSRkGER6IZ4UcCFu92A8ReCrpuNROgDsmjPcXNUQsJJ1iFxLeR5nWj3X_A1wko1UhicGu5xTAQL9q3HBY1VXQ1pHaC3fjmByV34r--zvOzPuL27Bcw85MTj5rWjoYjuEvAjRJZcqd1YOha4p5HvXuD_aUd97OGqtHlutCqC8uHd7yFbPHZg2o7QEs16f1nHpsviUNtD_geElFfD3tCgotXUskh0VPPHRjmhO7DbvkAD0Jwtl2OVVZh6uuPtwl-r6U_aGgtobuyxH7jfaxvNYawO7Dr1Y0fI7Q6Q9DIMT4Ah7XfGkGVphCiMwO6gJn2C2nLOkgfhKXu_VGn5m-vuCvhhwZ_QByRSFR7eOIFR31kD9JhoXOn5Cs9a0_9dnS79GIkFIxZIe1douLImD4mQw-j_dcKBh_SNaXoUehl_e505HAJOFk7qXjpA-hIZEmG6GTMpveNNX493CcG1PENzY_HGiepTlHYxJoh2GmTVGZ95Mwu97FW7pTi83GEOxHzNEZmaBpt2_JvCDXV1Mya12kFEru71th5kb8W9kodsGczxdVFpfzx-1I4HEx53e9Mvpcfr8iY7nLcAkaf1L-yeLGXYJ52NvZALKXWF5fj7yc7Wah-7qtn1dDVUA0BWbP_o997dplW9sKmV0k029dv4'
            ]
        )
            ->assertStatus(200);

    }
 }
